package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Blog;
@Repository
public interface BlogRepository extends JpaRepository<Blog,Integer>{
	
	List<Blog> findAllByCatId(int get);

	
	@Query("SELECT b FROM Blog b ORDER BY b.likes DESC LIMIT 4")
    List<Blog> findTop4MostPopularArticles();
	
	@Query("SELECT b FROM Blog b ORDER BY b.likes DESC ")
    List<Blog> findAllMostPopularArticles();
	
	@Query("SELECT b FROM Blog b WHERE b.catId = :catId ORDER BY b.likes DESC LIMIT 4")
    List<Blog> findTop4ByJavaOrderByLikesDesc(int catId);
	
	@Query("SELECT b FROM Blog b WHERE b.catId = :catId ORDER BY b.likes DESC LIMIT 4")
	List<Blog> findTop4ByPythonOrderByLikesDesc(int catId);
	
	@Query("SELECT b FROM Blog b WHERE b.catId = :catId ORDER BY b.likes DESC LIMIT 4")
	List<Blog> findTop4ByWebtechOrderByLikesDesc(int catId);
	
	@Query("SELECT b FROM Blog b WHERE b.catId = :catId ORDER BY b.likes DESC LIMIT 4")
	List<Blog> findTop4ByDbOrderByLikesDesc(int catId);
	
	@Query("SELECT b FROM Blog b WHERE b.catId = :catId ORDER BY b.likes DESC")
	List<Blog> findAllJavaOrderByLikesDesc(int catId);
	
	@Query("SELECT b FROM Blog b WHERE b.catId = :catId ORDER BY b.likes DESC")
	List<Blog> findAllPythonOrderByLikesDesc(int catId);
	
	@Query("SELECT b FROM Blog b WHERE b.catId = :catId ORDER BY b.likes DESC")
	List<Blog> findAllWebtechOrderByLikesDesc(int catId);
	
	@Query("SELECT b FROM Blog b WHERE b.catId = :catId ORDER BY b.likes DESC")
	List<Blog> findAllDbOrderByLikesDesc(int catId);
	
	List<Blog> findByBlogTitle(String blogTitle);
	
	List<Blog> findByUserId(int userId);
	
	

	
}
