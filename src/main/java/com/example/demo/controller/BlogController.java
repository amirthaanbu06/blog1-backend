package com.example.demo.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.BlogDTO;
import com.example.demo.model.Blog;
import com.example.demo.model.UserTable;
import com.example.demo.model.responceVo;
import com.example.demo.service.BlogService;

@CrossOrigin(origins="http://localhost:4200")
@RestController
@RequestMapping("/restController1")
public class BlogController {
	
	@Autowired
	BlogService blogService;
	
	
	@PostMapping("/register")
	public UserTable addData(@RequestBody UserTable reg) {
//		System.out.println(1111111111);
		return this.blogService.addRegisterService(reg);
	}
	@PostMapping("/login")
	public responceVo logindata1(@RequestBody UserTable login) {
		return this.blogService.logindata2(login);
	}
	
	@PostMapping("/createblog")
	public Blog addBlog(@RequestBody Blog post) {
		Blog addedBlog = blogService.addBlogService(post);
		return addedBlog;
	}
	
	
	@GetMapping("/home/popular_articles")
    public List<BlogDTO> getTop4MostPopularArticles() {
        return blogService.getTop4MostPopularArticles();
    }
	
	@GetMapping("/home/popular/see_all_articles")
    public List<BlogDTO> getAllMostPopularArticles() {
//		System.out.println(555555);
        return blogService.getAllMostPopularArticles();
    }
	
	
	
	@GetMapping("/home/java")
    public List<BlogDTO> getTop4JavaBlogs() {
        return blogService.getTop4JavaBlogsByLikes();
    }
	@GetMapping("/home/python")
	public List<BlogDTO> getTop4PythonBlogs() {
		return blogService.getTop4PythonBlogsByLikes();
	}
	@GetMapping("/home/webtech")
	public List<BlogDTO> getTop4WebtechBlogs() {
		return blogService.getTop4WebtechBlogsByLikes();
	}
	@GetMapping("/home/database")
	public List<BlogDTO> getTop4DbBlogs() {
		return blogService.getTop4DbBlogsByLikes();
	}
	
	@GetMapping("/home/java/see_all_articles")
	public List<BlogDTO> getAllJavaBlogsByLikes() {
		System.out.println(3333333);
		return blogService.getAllJavaBlogsByLikes();
	}
	@GetMapping("/home/python/see_all_articles")
	public List<BlogDTO> getAllPythonBlogsByLikes() {
		return blogService.getAllPythonBlogsByLikes();
	}
	@GetMapping("/home/webtech/see_all_articles")
	public List<BlogDTO> getAllwebtechBlogsByLikes() {
		return blogService.getAllWebtechBlogsByLikes();
	}
	@GetMapping("/home/database/see_all_articles")
	public List<BlogDTO> getAllDbBlogsByLikes() {
		return blogService.getAllDbBlogsByLikes();
	}
	
//	same query so no need of service and repo impl. only end points changes
	@GetMapping("/home/category/java")
	public List<BlogDTO> getAllJavaBlogsByLikes1() {
		
		return blogService.getAllJavaBlogsByLikes();
	}
	@GetMapping("/home/category/python")
	public List<BlogDTO> getAllPythonBlogsByLikes1() {
		return blogService.getAllPythonBlogsByLikes();
	}
	@GetMapping("/home/category/webtech")
	public List<BlogDTO> getAllwebtechBlogsByLikes1() {
		return blogService.getAllWebtechBlogsByLikes();
	}
	@GetMapping("/home/category/db")
	public List<BlogDTO> getAllDbBlogsByLikes1() {
		return blogService.getAllDbBlogsByLikes();
	}
	
	
	
	
	// this the method to format the timestamp as needed.
	private String formatTimestamp(String timestamp) {
	    try {
	        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	        SimpleDateFormat outputFormat = new SimpleDateFormat("MMM dd yyyy");

	        Date date = inputFormat.parse(timestamp);
	        String formattedTimestamp = outputFormat.format(date);

	        return formattedTimestamp;
	    } catch (ParseException e) {
	        e.printStackTrace();
	        
	        return "Invalid Timestamp"; 
	    }
	}
	
	
	
//	@GetMapping("/home/blogs/{blogTitle}")
//    public BlogDTO getBlogByTitle(@PathVariable String blogTitle) {
//		System.out.println(11111);
//        Blog blog = blogService.getBlogByTitle(blogTitle); 
//        if (blog == null) {
//        	System.out.println(22222);
//            // Handle not found case, return an appropriate response or status.
//            return null;
//        }
//
//        String formattedTimestamp = formatTimestamp(blog.getTimestamp()); 
//
//        BlogDTO response = new BlogDTO(
//            blog.getBlogId(),
//            blog.getBlogTitle(),
//            blog.getBlogDescription(),
//            blog.getLikes(),
//            blog.getComments(),
//            blog.getAuthor(),
//            formattedTimestamp,
//            blog.getCatId()
//        		);
//        System.out.println(response.getBlogTitle());
//        return response;
//    }
	
	@GetMapping("/home/blogs/{blogTitle}")
	public List<BlogDTO> getBlogsByTitle(@PathVariable String blogTitle) {
	    List<Blog> blogs = blogService.getBlogsByTitle(blogTitle); 

	    if (blogs == null || blogs.isEmpty()) {
	        
	        return new ArrayList<>(); // Return an empty list.
	    }

	    List<BlogDTO> response = new ArrayList<>();

	    for (Blog blog : blogs) {
	        String formattedTimestamp = formatTimestamp(blog.getTimestamp()); 
	        BlogDTO blogDTO = new BlogDTO(
	            blog.getBlogId(),
	            blog.getBlogTitle(),
	            blog.getBlogDescription(),
	            blog.getLikes(),
	            blog.getComments(),
	            blog.getAuthor(),
	            formattedTimestamp,
	            blog.getCatId()
	        );

	        response.add(blogDTO);
	    }

	    return response;
	}
	
	@GetMapping("/home/my_blogs/{userId}")
    public List<Blog> getUserBlogs(@PathVariable int userId) {
        return blogService.getUserBlogs(userId);
    }


}
