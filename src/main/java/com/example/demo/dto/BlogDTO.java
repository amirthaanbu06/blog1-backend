package com.example.demo.dto;

public class BlogDTO {
	
	private int blogId;
    private String blogTitle;
    private String blogDescription;
    private int likes;
    private int comments;
    private String author;
    private String timestamp;
    private int catId;
    private int userId;
    public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
	private String formattedTimestamp;
    
    public BlogDTO(int blogId, String blogTitle, String blogDescription, int likes, int comments, String author, String formattedTimestamp) {
        this.blogId = blogId;
        this.blogTitle = blogTitle;
        this.blogDescription = blogDescription;
        this.likes = likes;
        this.comments = comments;
        this.author = author;
        this.formattedTimestamp = formattedTimestamp;
    }
    
    public BlogDTO(int blogId, String blogTitle, String blogDescription, int likes, int comments, String author, String formattedTimestamp, int catId) {
        this.blogId = blogId;
        this.blogTitle = blogTitle;
        this.blogDescription = blogDescription;
        this.likes = likes;
        this.comments = comments;
        this.author = author;
        this.formattedTimestamp = formattedTimestamp;
        this.catId = catId; 
    }
	
	public int getBlogId() {
		return blogId;
	}
	public void setBlogId(int blogId) {
		this.blogId = blogId;
	}
	public String getBlogTitle() {
		return blogTitle;
	}
	public void setBlogTitle(String blogTitle) {
		this.blogTitle = blogTitle;
	}
	public String getBlogDescription() {
		return blogDescription;
	}
	public void setBlogDescription(String blogDescription) {
		this.blogDescription = blogDescription;
	}
	public int getLikes() {
		return likes;
	}
	public void setLikes(int likes) {
		this.likes = likes;
	}
	public int getComments() {
		return comments;
	}
	public void setComments(int comments) {
		this.comments = comments;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public int getCatId() {
		return catId;
	}
	public void setCatId(int catId) {
		this.catId = catId;
	}
	public String getFormattedTimestamp() {
		return formattedTimestamp;
	}
	public void setFormattedTimestamp(String formattedTimestamp) {
		this.formattedTimestamp = formattedTimestamp;
	}
    
	
    
    

}
